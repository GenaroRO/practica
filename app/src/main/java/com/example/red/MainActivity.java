package com.example.red;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.red.Api.Api;
import com.example.red.Api.Servicios.ServicioPeticion;
import com.example.red.viewmodels.Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Button btnCrear;
    private String APITOKEN ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnCrar = (Button) findViewById(R.id.btnCrear);
        btnCrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,CrearCuenta.class);
                startActivityForResult(intent,0);
            }
        });
        //CODIGO INICIO SECION
        Button btnIniciarSecion =(Button)findViewById(R.id.btnIniciarSecion);
        btnIniciarSecion.setOnClickListener(new View.OnClickListener() {
            EditText Correo = (EditText)findViewById(R.id.Correo);
            EditText password = (EditText)findViewById(R.id.password);
            @Override
            public void onClick(View v) {
                ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
                Call<Login> loginCall =  service.getLogin(Correo.getText().toString(),password.getText().toString());
                loginCall.enqueue(new Callback<Login>() {
                    @Override
                    public void onResponse(Call<Login> call, Response<Login> response) {
                        Login peticion = response.body();
                        if(peticion.estado == "true"){
                            APITOKEN = peticion.token;
                            guardarPreferencias();
                            Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(MainActivity.this, InicioActivity.class));
                        }else{
                            Toast.makeText(MainActivity.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<Login> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Erro :(", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    public void guardarPreferencias () {
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }
}

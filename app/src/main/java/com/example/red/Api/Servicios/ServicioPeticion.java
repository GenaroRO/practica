package com.example.red.Api.Servicios;

import com.example.red.viewmodels.Login;
import com.example.red.viewmodels.ObtenerUsuarios;
import com.example.red.viewmodels.RegistroUsuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<RegistroUsuario> registrarusuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/todosUsuarios")
    Call<ObtenerUsuarios>obtenerUsuario(@Field("token") String token);


}

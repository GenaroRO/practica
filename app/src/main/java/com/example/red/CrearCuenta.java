package com.example.red;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.red.Api.Api;
import com.example.red.Api.Servicios.ServicioPeticion;
import com.example.red.viewmodels.RegistroUsuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrearCuenta extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);

        Button btnCrearUsuario = (Button) findViewById(R.id.btnCrearUsuario);
        btnCrearUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText UsuarioNu = (EditText)findViewById(R.id.UsuarioNu);
                EditText password1 = (EditText) findViewById(R.id.password1);
                EditText password2 = (EditText)findViewById(R.id.password2);

                ServicioPeticion service = Api.getApi(CrearCuenta.this).create(ServicioPeticion.class);
                Call<RegistroUsuario> registrarCall =  service.registrarusuario(UsuarioNu.getText().toString(),password1.getText().toString());
                registrarCall.enqueue(new Callback<RegistroUsuario>() {
                    @Override
                    public void onResponse(Call<RegistroUsuario> call, Response<RegistroUsuario> response) {
                        RegistroUsuario peticion = response.body();
                        if(response.body() == null){
                            Toast.makeText(CrearCuenta.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(peticion.estado == "true"){
                            startActivity(new Intent(CrearCuenta.this,MainActivity.class));
                            Toast.makeText(CrearCuenta.this, "Muy bien Dato Registrado", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(CrearCuenta.this, peticion.detalle, Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<RegistroUsuario> call, Throwable t) {
                        Toast.makeText(CrearCuenta.this, "Erro :(", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
